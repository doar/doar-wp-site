<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/var/www/html/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'doar_wp1');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'rajeev');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'I6Yjz2QpZVDX6kuUl10nHy4h6DDb6GyPuEWoqx5qAOfv2sE9DPQrMOY1aAM4RaXU');
define('SECURE_AUTH_KEY',  'DyWq8kBVM6G0yC0Gm7lpqKqFWC087Rz2BwFmJjgMFJIXVHXgknbR5zXSQDdGPPIw');
define('LOGGED_IN_KEY',    'pkL7YmfrgPkMyD4vwvNLJL6i2CcaIONkPPXgFaJjb3SyNYsvrKkWu4fAxZzlrIGe');
define('NONCE_KEY',        'irtRgyUpxjNaH9fajHniUXLr1SoI8pvyHRDhqizVbuzLDeZ3hfHR2PLnYXjpJNrS');
define('AUTH_SALT',        'cpAvnNgKEpOypxyjzkHJnhDb3TwSUPv3RkHaS9HwLcaqMexVv26LwB9XvXbNZT1y');
define('SECURE_AUTH_SALT', 'jhrY5UcwGkGzyPfXxD2YSiU1O05B4YQikVPyTy3xCDpPJr6pO6oOZZAkMY2hUtos');
define('LOGGED_IN_SALT',   '2GF8uEqVrMhk80btsJ2be4wkTfMh4qfK0PTxPaPDLpYhqgKI3iI3d6xQmNyXo9p2');
define('NONCE_SALT',       'LPQQAVb77MAyIs2vODFX4aKcqSaTL4u43JV0ofe85Ql64lk46NA2UJtHYgvHR8iD');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);
define('WP_HOME','http://localhost');
define('WP_SITEURL','http://localhost');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
