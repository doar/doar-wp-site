<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'doar_wp2');

/** MySQL database username */
define('DB_USER', 'doar_wp2');

/** MySQL database password */
define('DB_PASSWORD', 'W&&&@p0Oq88pU~(ICD.02*&9');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1ushSyVGLr6wCfmPcOoVbXOxAsSiJcvuam04Xdytvmc3Smz65PDcSOHC1HWmgVJ4');
define('SECURE_AUTH_KEY',  '9oHNWBZvKSQKSq8Gc6gIAxB6GHkRs2TDg5lYsZM9heopCuv6HceBqivPaUh05sV6');
define('LOGGED_IN_KEY',    'AWSYjrUyGxuBFpID1nNQKQQFY2lXkyo2DdGO04FGiOwA9K1DZq2DYHpx9cMGJBEu');
define('NONCE_KEY',        'FJUgElgpH3QJYeQGKz7xTVF70pXZMNfaZr9tF79IEPRWoLZZvOVavb7KE4bKKC9x');
define('AUTH_SALT',        'k4Xk3pNygZoT2eBnc0aCD4RMDQmISOYP8G5kGiWRukzNIAm24TAywl7EU5uVd6ol');
define('SECURE_AUTH_SALT', 'XgsbqmsK1hY5CZklwbHliMzlHsyhZT3v5d3xVdvY60673ZaXwY3hI5kB23qoR8WX');
define('LOGGED_IN_SALT',   'sOHUIRxCc1FHpgNslaoTdJycm7lwESxov2HslMTcSP6usvnJULUS0EdUCMYATO7Z');
define('NONCE_SALT',       'WkDNjo3y6nPbVxWAwKqrMklzQZCFH9d43iiU3yr8Q84Fk64BljvGxLC0ssDhBaA5');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
