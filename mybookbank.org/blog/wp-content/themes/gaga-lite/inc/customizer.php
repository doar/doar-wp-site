<?php
/**
 * gaga lite Theme Customizer
 *
 * @package gaga lite
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function gaga_lite_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'gaga_lite_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function gaga_lite_customize_preview_js() {
	wp_enqueue_script( 'gaga_lite_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'gaga_lite_customize_preview_js' );

// Our Product detail's
if(class_exists( 'WP_Customize_control')){
    class Theme_Info_Product_Custom_Control extends WP_Customize_Control
    {
        public function render_content()
        {
            ?>
            <label>
                <!-- <h3 class="customize-title"><?php //echo esc_html( $this->label ); ?></h3>
                <br /> -->
                <span class="customize-text_editor_desc button">
                    <?php echo wp_kses_post( $this->description ); ?>
                </span>
            </label>
            <?php
        }
    }
}