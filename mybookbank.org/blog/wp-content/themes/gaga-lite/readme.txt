License: GNU General Public License v3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Install Steps:
--------------

1. Activate the theme
2. Go to the Customizer Page
3. Setup customizer options


------------------------------------------

JS Files
    
    BXSlider by Steven Wanderski: 
    License: WTFPL and MIT license: WTFPL and MIT license
    https://github.com/stevenwanderski/bxslider-4
	
	Classy Loader(Circular-percentage-loader)
    License:MIT www.class.pm/LICENSE-MIT
    https://vox.space/files/jquery/classyloader/
    2005 - 2015 � vox.SPACE
    By Marius Stanciu
    
    JQuery One Page Nav Plugin
    License: MIT and GPL
    http://github.com/davist11/jQuery-One-Page-Nav
    By:Trevor Davis
    
    Localscroll
    License: MIT
    https://github.com/flesler/jquery.localScroll
    Copyright (c) 2007-2015 Ariel Flesler
    
    Waypoints
    License: MIT License
    https://github.com/imakewebthings/waypoints
    Copyright � 2011-2015 Caleb Troughton
    
    Parallax JS 
    License: MIT and GPL licenses
    https://github.com/IanLunn/jQuery-Parallax/
    By: Ian Lunn
    
    WOW JS
    License: MIT Licenses
    https://github.com/matthieua/WOW
    
    Flexisel
    License: MIT License
    https://github.com/9bitStudios/flexisel
    
    Jquery.scrollTo
    license: MIT License
    https://github.com/flesler/jquery.scrollTo/
    By Ariel Flesler
    
    Jquery.nav
    License:the MIT and GPL
    http://github.com/davist11/jQuery-One-Page-Nav
    Copyright (c) 2010 Trevor Davis (http://trevordavis.net)
-----------------------------------------------------------
CSS File
    BXSlider CSS
    by Steven Wanderski: 
    License: WTFPL and MIT license: WTFPL and MIT license
    https://github.com/stevenwanderski/bxslider-4
        
    Flexisel CSS
    License: MIT License
    https://github.com/9bitStudios/flexisel
    
    WOW CSS
    License: MIT Licenses
    https://github.com/matthieua/WOW
-------------------------------------------
Fonts
    Font Awesome 4.4.0 by @davegandy - http://fontawesome.io - @fontawesome
    License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)
        
    Lato, by Warsaw-based designer Lukasz Dziedzic: SIL Open Font License, 1.1  
    https://www.google.com/fonts/specimen/Lato
    
    Oswald by Alternate Gothic' sans serif typefaces: SIL Open Font License, 1.1
    https://www.google.com/fonts/specimen/Oswald
    
    Arvo by Anton Koovit: SIL Open Font License, 1.1
    https://www.google.com/fonts/specimen/Arvo    
   
---------------------------------------------
Images
    All the images used are is designed by AccessPress team.
        
---------------------------------------------
Documentation
    Setup Home Page
        -Go Appearance -> Customizer -> General Setting and enable "Enable one page site"
    
    Add Parallax Section
        -Go to Appearance -> Customizer -> Home Page Setting. Enable the section and configure the necessary setting.

---------------------------------------------
Version 1.0.7
* Minor Design and Programming Bug fixex
* More Themes Page added
* Responsive bug fixed


Version 1.0
* Submitted theme for review in http://wordpress.org