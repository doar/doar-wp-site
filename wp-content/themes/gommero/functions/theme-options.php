<?php

add_action('init', 'of_options');

if (!function_exists('of_options')) {

    function of_options() {
        // VARIABLES
        $themename = function_exists('wp_get_theme') ? wp_get_theme() : wp_get_theme();
        $themename = $themename['Name'];
        $shortname = "of";
        // Test data
        $test_array = array("one" => "One", "two" => "Two", "three" => "Three", "four" => "Four", "five" => "Five");

        // Multicheck Array
        $multicheck_array = array("one" => "French Toast", "two" => "Pancake", "three" => "Omelette", "four" => "Crepe", "five" => "Waffle");

        // Multicheck Defaults
        $multicheck_defaults = array("one" => "1", "five" => "1");

        // Background Defaults

        $background_defaults = array('color' => '', 'image' => '', 'repeat' => 'repeat', 'position' => 'top center', 'attachment' => 'scroll');


        // Pull all the categories into an array
        $options_categories = array();
        $options_categories_obj = get_categories();
        foreach ($options_categories_obj as $category) {
            $options_categories[$category->cat_ID] = $category->cat_name;
        }

        // Pull all the pages into an array
        $options_pages = array();
        $options_pages_obj = get_pages('sort_column=post_parent,menu_order');
        $options_pages[''] = 'Select a page:';
        foreach ($options_pages_obj as $page) {
            $options_pages[$page->ID] = $page->post_title;
        }

        // If using image radio buttons, define a directory path
        $imagepath = get_template_directory_uri() . '/images/';

        $options = array(
            array("name" => __("General Settings", 'gommero'),
                "type" => "heading"),
            array("name" => __("Custom Logo", 'gommero'),
                "desc" => __("Choose your own logo. Optimal Size: 170px Wide by 30px Height", 'gommero'),
                "id" => "gommero_logo",
                "type" => "upload"),
            array("name" => __("Custom Favicon", 'gommero'),
                "desc" => __("Specify a 16px x 16px image that will represent your website's favicon.", 'gommero'),
                "id" => "gommero_favicon",
                "type" => "upload"),
//****=============================================================================****//
//****-----------This code is used for creating slider settings--------------------****//							
//****=============================================================================****//						
            array("name" => __("Home Page Image Settings", 'gommero'),
                "type" => "heading"),
            array("name" => __("Home Page Image", 'gommero'),
                "desc" => __("Choose Image for your Home Page Image. Optimal Size: 900px x 350px", 'gommero'),
                "id" => "gommero_slideimage1",
                "std" => "",
                "type" => "upload"),
            array("name" => __("Home Page Image Link", 'gommero'),
                "desc" => __("Enter your link url for Home Page Image", 'gommero'),
                "id" => "gommero_slidelink1",
                "std" => "",
                "type" => "text"),
//****=============================================================================****//
//****-----------This code is used for creating home page feature content----------****//							
//****=============================================================================****//	
            array("name" => __("Home Page Settings", 'gommero'),
                "type" => "heading"),
            array("name" => __("Home Page Intro", 'gommero'),
                "desc" => __("Enter your heading text for home page", 'gommero'),
                "id" => "gommero_mainheading",
                "std" => "",
                "type" => "textarea"),
            //***Code for first column***//
            array("name" => __("First Feature Image", 'gommero'),
                "desc" => __("Choose image for your feature column first. Optimal size 198px x 115px", 'gommero'),
                "id" => "gommero_fimg1",
                "std" => "",
                "type" => "upload"),
            array("name" => __("First Feature Heading", 'gommero'),
                "desc" => __("Enter your heading line for first column", 'gommero'),
                "id" => "gommero_headline1",
                "std" => "",
                "type" => "textarea"),
            array("name" => __("First Feature Link", 'gommero'),
                "desc" => __("Enter your link for feature column first", 'gommero'),
                "id" => "gommero_link1",
                "std" => "",
                "type" => "text"),
            array("name" => __("First Feature Content", 'gommero'),
                "desc" => __("Enter your feature content for column first", 'gommero'),
                "id" => "gommero_feature1",
                "std" => "",
                "type" => "textarea"),
            //***Code for second column***//	
            array("name" => __("Second Feature Image", 'gommero'),
                "desc" => __("Choose image for your feature column second. Optimal size 198px x 115px", 'gommero'),
                "id" => "gommero_fimg2",
                "std" => "",
                "type" => "upload"),
            array("name" => __("Second Feature Heading", 'gommero'),
                "desc" => __("Enter your heading line for second column", 'gommero'),
                "id" => "gommero_headline2",
                "std" => "",
                "type" => "textarea"),
            array("name" => __("Second Feature Link", 'gommero'),
                "desc" => __("Enter your link for feature column second", 'gommero'),
                "id" => "gommero_link2",
                "std" => "",
                "type" => "text"),
            array("name" => __("Second Feature Content", 'gommero'),
                "desc" => __("Enter your feature content for column second", 'gommero'),
                "id" => "gommero_feature2",
                "std" => "",
                "type" => "textarea"),
            //***Code for third column***//	
            array("name" => __("Third Feature Image", 'gommero'),
                "desc" => __("Choose image for your feature column thrid. Optimal size 198px x 115px", 'gommero'),
                "id" => "gommero_fimg3",
                "std" => "",
                "type" => "upload"),
            array("name" => __("Third Feature Heading", 'gommero'),
                "desc" => __("Enter your heading line for third column", 'gommero'),
                "id" => "gommero_headline3",
                "std" => "",
                "type" => "textarea"),
            array("name" => __("Third Feature Link", 'gommero'),
                "desc" => __("Enter your link for feature column third", 'gommero'),
                "id" => "gommero_link3",
                "std" => "",
                "type" => "text"),
            array("name" => __("Third Feature Content", 'gommero'),
                "desc" => __("Enter your feature content for third column", 'gommero'),
                "id" => "gommero_feature3",
                "std" => "",
                "type" => "textarea"),
            array("name" => __("Posts On Front Page", 'gommero'),
                "desc" => __("Check if you want to show latest posts on front page.", 'gommero'),
                "id" => "gommero_post_front",
                "std" => "1",
                "type" => "checkbox"),
//****=============================================================================****//
//****-----------This code is used for creating custom css options----------****//							
//****=============================================================================****//				
            array("name" => __("Styling Options", 'gommero'),
                "type" => "heading"),
            array("name" => __("Custom CSS", 'gommero'),
                "desc" => __("Quickly add some CSS to your theme by adding it to this block.", 'gommero'),
                "id" => "gommero_customcss",
                "std" => "",
                "type" => "textarea"),
//****=============================================================================****//
//****-----------This code is used for creating Social Options--------------****//							
//****=============================================================================****//
            array("name" => __("Social Network Icons", 'gommero'),
                "type" => "heading"),
            array("name" => "Facebook URL",
                "desc" => __("Enter your Facebook URL if you have one", 'gommero'),
                "id" => "gommero_facebook",
                "std" => "",
                "type" => "text"),
            array("name" => "Twitter URL",
                "desc" => __("Enter your Twitter URL if you have one", 'gommero'),
                "id" => "gommero_twitter",
                "std" => "",
                "type" => "text"),
            array("name" => "RSS URL",
                "desc" => __("Enter your RSS Feed URL if you have one", 'gommero'),
                "id" => "gommero_rss",
                "std" => "",
                "type" => "text"));

        gommero_update_option('of_template', $options);
        gommero_update_option('of_themename', $themename);
        gommero_update_option('of_shortname', $shortname);
    }

}
?>
