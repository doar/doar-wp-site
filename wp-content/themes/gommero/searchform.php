<form role="search" method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>">
    <div>
        <input type="text" onfocus="if (this.value == 'Search') {
                    this.value = '';
                }" onblur="if (this.value == '') {
                            this.value = <?php _e('Search', 'gommero'); ?>;
                        }"  value="Search" name="s" id="s" />
        <input type="submit" id="searchsubmit" value="<?php _e('Search', 'gommero'); ?>" />
    </div>
</form>
