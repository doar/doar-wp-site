<div class="grid_8 alpha">
    <div class="inner first">
        <?php if (is_active_sidebar('first-footer-widget-area')) : ?>
            <?php dynamic_sidebar('first-footer-widget-area'); ?>
        <?php else : ?>
            <h4><?php _e('Setting Footer Widgets', 'gommero'); ?></h4>
            <?php _e('Footer is widgetized. To setup the footer, drag the required Widgets in Appearance -> Widgets Tab in the First, Second or Third Footer Widget Areas.', 'gommero'); ?>
        <?php endif; ?>
    </div>
</div>
<div class="grid_8">
    <div class="inner">
        <?php if (is_active_sidebar('second-footer-widget-area')) : ?>
            <?php dynamic_sidebar('second-footer-widget-area'); ?>
        <?php else : ?>
            <h4><?php _e('Archives Widget', 'gommero'); ?></h4>
            <ul>
                <li><a href="#"><?php _e('January', 'gommero'); ?></a></li>
                <li><a href="#"><?php _e('February', 'gommero'); ?></a></li>
                <li><a href="#"><?php _e('March', 'gommero'); ?></a></li>
                <li><a href="#"><?php _e('April', 'gommero'); ?></a></li>
                <li><a href="#"><?php _e('August', 'gommero'); ?></a></li>
                <li><a href="#"><?php _e('September', 'gommero'); ?></a></li>
            </ul>
        <?php endif; ?>
    </div>
</div>
<div class="grid_8 omega">
    <div class="inner">
        <?php if (is_active_sidebar('third-footer-widget-area')) : ?>
            <?php dynamic_sidebar('third-footer-widget-area'); ?>
        <?php else : ?>
            <h4><?php _e('All Links', 'gommero'); ?></h4>
            <ul>
                <li><a href="#"><?php _e('Home', 'gommero'); ?></a></li>
                <li><a href="#"><?php _e('About Us', 'gommero'); ?></a></li>
                <li><a href="#"><?php _e('Blog', 'gommero'); ?></a></li>
                <li><a href="#"><?php _e('Gallery', 'gommero'); ?></a></li>
                <li><a href="#"><?php _e('Contact us', 'gommero'); ?></a></li>
            </ul>
        <?php endif; ?>
    </div>
</div>
