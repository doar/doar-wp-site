<?php die(); ?><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en-GB" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="en-GB" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en-GB" prefix="og: http://ogp.me/ns#"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en-GB" prefix="og: http://ogp.me/ns#"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
    <title>Utthan (उत्थान) - Donate an Hour ( DoaR India)</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<link rel="shortcut icon" type="image/x-icon" href="http://www.donateanhour.org/wp-content/uploads/2015/06/cropped-new1-copy1.jpg" />
  	  
<!-- This site is optimized with the Yoast SEO plugin v3.1.1 - https://yoast.com/wordpress/plugins/seo/ -->
<!-- Admin only notice: this page doesn't show a meta description because it doesn't have one, either write it for this page specifically or go into the SEO -> Titles menu and set up a template. -->
<link rel="canonical" href="http://localhost/programs/uthaan/" />
<meta property="og:locale" content="en_GB" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Utthan (उत्थान) - Donate an Hour ( DoaR India)" />
<meta property="og:description" content="&#8220;We rise by lifting others&#8221; Overview Utthan means upliftment. This is our flagship program that aims to provide supplementary of parental support to the children from poor and disadvantaged socio-economic background. Under this program volunteers across Delhi NCR run daily or weekend classes &hellip; Continued" />
<meta property="og:url" content="http://localhost/programs/uthaan/" />
<meta property="og:site_name" content="Donate an Hour ( DoaR India)" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:description" content="&#8220;We rise by lifting others&#8221; Overview Utthan means upliftment. This is our flagship program that aims to provide supplementary of parental support to the children from poor and disadvantaged socio-economic background. Under this program volunteers across Delhi NCR run daily or weekend classes &hellip; Continued" />
<meta name="twitter:title" content="Utthan (उत्थान) - Donate an Hour ( DoaR India)" />
<!-- / Yoast SEO plugin. -->

<link rel="alternate" type="application/rss+xml" title="Donate an Hour ( DoaR India) &raquo; Feed" href="http://localhost/feed/" />
<link rel="alternate" type="application/rss+xml" title="Donate an Hour ( DoaR India) &raquo; Comments Feed" href="http://localhost/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="Donate an Hour ( DoaR India) &raquo; Utthan (उत्थान) Comments Feed" href="http://localhost/programs/uthaan/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/localhost\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.4.2"}};
			!function(a,b,c){function d(a){var c,d=b.createElement("canvas"),e=d.getContext&&d.getContext("2d"),f=String.fromCharCode;return e&&e.fillText?(e.textBaseline="top",e.font="600 32px Arial","flag"===a?(e.fillText(f(55356,56806,55356,56826),0,0),d.toDataURL().length>3e3):"diversity"===a?(e.fillText(f(55356,57221),0,0),c=e.getImageData(16,16,1,1).data.toString(),e.fillText(f(55356,57221,55356,57343),0,0),c!==e.getImageData(16,16,1,1).data.toString()):("simple"===a?e.fillText(f(55357,56835),0,0):e.fillText(f(55356,57135),0,0),0!==e.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag"),unicode8:d("unicode8"),diversity:d("diversity")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag&&c.supports.unicode8&&c.supports.diversity||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='open-sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&#038;subset=latin%2Clatin-ext&#038;ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css'  href='http://localhost/wp-includes/css/dashicons.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='admin-bar-css'  href='http://localhost/wp-includes/css/admin-bar.min.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='fontsforwebstyle-css'  href='http://localhost/wp-content/plugins/font/css/fontsforwebstyle.css?pver=7.5.1&#038;ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='jquery-ui-css'  href='http://localhost/wp-content/plugins/font/css/start/jquery-ui-1.8.14.custom.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='colorpicker2-css'  href='http://localhost/wp-content/plugins/font/css/colorpicker.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='http://localhost/wp-content/plugins/revslider/rs-plugin/css/settings.css?rev=4.6.0&#038;ver=4.4.2' type='text/css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
.tp-caption a{color:#ff7302;text-shadow:none;-webkit-transition:all 0.2s ease-out;-moz-transition:all 0.2s ease-out;-o-transition:all 0.2s ease-out;-ms-transition:all 0.2s ease-out}.tp-caption a:hover{color:#ffa902}
</style>
<link rel='stylesheet' id='team_front_style-css'  href='http://localhost/wp-content/plugins/team/css/style.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='boxes-css'  href='http://localhost/wp-content/plugins/wordpress-seo/css/adminbar-302.min.css?ver=3.1.1' type='text/css' media='all' />
<link rel='stylesheet' id='kadence_theme-css'  href='http://localhost/wp-content/themes/virtue/assets/css/virtue.css?ver=257' type='text/css' media='all' />
<link rel='stylesheet' id='virtue_skin-css'  href='http://localhost/wp-content/themes/virtue/assets/css/skins/default.css' type='text/css' media='all' />
<link rel='stylesheet' id='redux-google-fonts-virtue-css'  href='http://fonts.googleapis.com/css?family=Oxygen%3A700%7CVibur%3A400%7CLato%3A700%2C400italic%2C400&#038;subset=latin%2Clatin-ext&#038;ver=1458222761' type='text/css' media='all' />
<script type='text/javascript' src='http://localhost/wp-includes/js/jquery/jquery.js?ver=1.11.3'></script>
<script type='text/javascript' src='http://localhost/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>
<script type='text/javascript' src='http://localhost/wp-content/plugins/font/js/jquery.fcarousel.min.js?ver=4.4.2'></script>
<script type='text/javascript' src='http://localhost/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://localhost/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://localhost/wp-includes/js/jquery/ui/mouse.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://localhost/wp-includes/js/jquery/ui/draggable.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://localhost/wp-includes/js/jquery/ui/slider.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://localhost/wp-content/plugins/font/js/colorpicker.js?ver=4.4.2'></script>
<script type='text/javascript' src='http://localhost/wp-includes/js/jquery/ui/sortable.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://localhost/wp-content/plugins/font/js/jquery.fontPlugin.js?pver=7.5.1&#038;ver=4.4.2'></script>
<script type='text/javascript' src='http://localhost/wp-content/plugins/font/js/pluginscripts.js?pver=7.5.1&#038;ver=4.4.2'></script>
<script type='text/javascript' src='http://localhost/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.tools.min.js?rev=4.6.0&#038;ver=4.4.2'></script>
<script type='text/javascript' src='http://localhost/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.revolution.min.js?rev=4.6.0&#038;ver=4.4.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var team_ajax = {"team_ajaxurl":"http:\/\/localhost\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='http://localhost/wp-content/plugins/team/js/scripts.js?ver=4.4.2'></script>
<script type='text/javascript' src='http://localhost/wp-content/plugins/team/js/masonry.pkgd.min.js?ver=4.4.2'></script>
<script type='text/javascript' src='http://localhost/wp-content/themes/virtue/assets/js/vendor/modernizr.min.js'></script>
<script type='text/javascript' src='http://localhost/wp-content/plugins/google-analyticator/external-tracking.min.js?ver=6.4.9'></script>
<link rel='https://api.w.org/' href='http://localhost/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://localhost/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://localhost/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.4.2" />
<link rel='shortlink' href='http://localhost/?p=31' />
<link rel="alternate" type="application/json+oembed" href="http://localhost/wp-json/oembed/1.0/embed?url=http%3A%2F%2Flocalhost%2Fprograms%2Futhaan%2F" />
<link rel="alternate" type="text/xml+oembed" href="http://localhost/wp-json/oembed/1.0/embed?url=http%3A%2F%2Flocalhost%2Fprograms%2Futhaan%2F&#038;format=xml" />
<style type="text/css">PICK AN ELEMENT NOW - or type CSS selector(advanced) {
font-size: 30px !important;
color: #444 !important;
}
</style><style type="text/css">#logo {padding-top:0px;}#logo {padding-bottom:0px;}#logo {margin-left:0px;}#logo {margin-right:0px;}#nav-main {margin-top:0px;}#nav-main {margin-bottom:0px;}.headerfont, .tp-caption {font-family:Lato;} 
  .topbarmenu ul li {font-family:Lato;}
  #kadbreadcrumbs {font-family:Verdana, Geneva, sans-serif;}.product_item .product_details h5 {text-transform: none;}@media (max-width: 979px) {.nav-trigger .nav-trigger-case {position: static; display: block; width: 100%;}}.product_item .product_details h5 {min-height:40px;}.kad-topbar-left, .kad-topbar-left .topbarmenu {float:right;} .kad-topbar-left .topbar_social, .kad-topbar-left .topbarmenu ul, .kad-topbar-left .kad-cart-total,.kad-topbar-right #topbar-search .form-search{float:left}[class*="wp-image"] {-webkit-box-shadow: none;-moz-box-shadow: none;box-shadow: none;border:none;}[class*="wp-image"]:hover {-webkit-box-shadow: none;-moz-box-shadow: none;box-shadow: none;border:none;}div#homeheader {
    display: none;
}
.sliderclass {
   padding-bottom: 50;
      display: none;
}
.one-column #content {
width: auto;
}
.footer-widget .widget, .footer-widget {
margin-top:0;
}
#containerfooter {
padding-top: 0;
}
.footercredits {
padding-top: 0;
padding-bottom: 0;
}
.topbarmenu ul li a {
font-size: 18px;
}
#nav-main .sf-menu>li:last-child>ul, #nav-second .sf-menu>li:last-child>ul {
   left: 0 !Important;
}</style><meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="http://localhost/wp-content/plugins/js_composer/assets/css/vc-ie8.css" media="screen"><![endif]--><style type="text/css" media="print">#wpadminbar { display:none; }</style>
<style type="text/css" media="screen">
	html { margin-top: 32px !important; }
	* html body { margin-top: 32px !important; }
	@media screen and ( max-width: 782px ) {
		html { margin-top: 46px !important; }
		* html body { margin-top: 46px !important; }
	}
</style>
		<script type="text/javascript">
			var ajaxproxy = 'http://localhost/wp-admin/admin-ajax.php';
			var fontBlogUrl = 'http://localhost';
			var fontBlogName = 'Donate an Hour ( DoaR India)';
			var fontPluginVersion = '7.5.1';
		</script>
		<!--[if lt IE 9]>
<script src="http://localhost/wp-content/themes/virtue/assets/js/vendor/respond.min.js"></script>
<![endif]-->
<link rel="icon" href="http://www.donateanhour.org/wp-content/uploads/2015/06/cropped-new1-copy1-32x32.jpg" sizes="32x32" />
<link rel="icon" href="http://www.donateanhour.org/wp-content/uploads/2015/06/cropped-new1-copy1-192x192.jpg" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="http://www.donateanhour.org/wp-content/uploads/2015/06/cropped-new1-copy1-180x180.jpg" />
<meta name="msapplication-TileImage" content="http://www.donateanhour.org/wp-content/uploads/2015/06/cropped-new1-copy1-270x270.jpg" />
<!-- Google Analytics Tracking by Google Analyticator 6.4.9: http://wordpress.org/plugins/google-analyticator/ -->
	<!-- Tracking code is hidden, since the settings specify not to track admins. Tracking is occurring for non-admins. -->
<style type="text/css" title="dynamic-css" class="options-output">header #logo a.brand,.logofont{font-family:Oxygen;line-height:40px;font-weight:700;font-style:normal;color:#2844fc;font-size:40px;}.kad_tagline{font-family:Vibur;line-height:20px;font-weight:400;font-style:normal;color:#49b6ed;font-size:40px;}.product_item .product_details h5{font-family:Lato;line-height:20px;font-weight:700;font-style:normal;font-size:16px;}h1{font-family:Lato;line-height:40px;font-weight:400;font-style:italic;font-size:25px;}h2{font-family:Lato;line-height:40px;font-weight:normal;font-style:normal;font-size:20px;}h3{font-family:Lato;line-height:40px;font-weight:400;font-style:normal;font-size:28px;}h4{font-family:Lato;line-height:40px;font-weight:400;font-style:normal;font-size:24px;}h5{font-family:Lato;line-height:24px;font-weight:700;font-style:normal;font-size:18px;}body{font-family:Verdana, Geneva, sans-serif;line-height:20px;font-weight:400;font-style:normal;font-size:14px;}#nav-main ul.sf-menu a{font-family:Lato;line-height:18px;font-weight:400;font-style:italic;color:#0d6cbf;font-size:15px;}#nav-second ul.sf-menu a{font-family:Lato;line-height:22px;font-weight:400;font-style:normal;color:#2878bf;font-size:10px;}.kad-nav-inner .kad-mnav, .kad-mobile-nav .kad-nav-inner li a,.nav-trigger-case{font-family:Lato;line-height:20px;font-weight:400;font-style:normal;color:#096abf;font-size:12px;}</style></head>
  <body class="page page-id-31 page-child parent-pageid-11 page-template page-template-page-fullwidth page-template-page-fullwidth-php logged-in admin-bar no-customize-support wide uthaan wpb-js-composer js-comp-ver-4.4.2 vc_responsive">
    <div id="wrapper" class="container">
    <div id="kt-skip-link"><a href="#content">Skip to Main Content</a></div><header class="banner headerclass" role="banner">
  <section id="topbar" class="topclass">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6 kad-topbar-left">
          <div class="topbarmenu clearfix">
                                  <div class="topbar_social">
              <ul>
                <li><a href="http://www.donateanhour.org/" target="_self" title="Home" data-toggle="tooltip" data-placement="bottom" data-original-title="Home"><i class="icon-home "></i></a></li><li><a href="https://facebook.com/donateanhour" target="_blank" title="follow on Facebook" data-toggle="tooltip" data-placement="bottom" data-original-title="follow on Facebook"><i class="icon-facebook-sign "></i></a></li><li><a href="https://twitter.com/doarindia" target="_self" title="follow on Twitter" data-toggle="tooltip" data-placement="bottom" data-original-title="follow on Twitter"><i class="icon-twitter "></i></a></li><li><a href="http://doarindia.blogspot.in/" target="_blank" title="Blog" data-toggle="tooltip" data-placement="bottom" data-original-title="Blog"><i class="icon-google-plus-sign "></i></a></li>              </ul>
            </div>
                                </div>
        </div><!-- close col-md-6 --> 
        <div class="col-md-6 col-sm-6 kad-topbar-right">
          <div id="topbar-search" class="topbar-widget">
                    </div>
        </div> <!-- close col-md-6-->
      </div> <!-- Close Row -->
    </div> <!-- Close Container -->
  </section>
  <div class="container">
    <div class="row">
          <div class="col-md-12 clearfix kad-header-left">
            <div id="logo" class="logocase">
              <a class="brand logofont" href="http://localhost/">
                                  <div id="thelogo">
                    <img src="http://www.donateanhour.org/wp-content/uploads/2015/07/Doar_logo_updated.jpg" alt="Donate an Hour ( DoaR India)" class="kad-standard-logo" />
                                      </div>
                              </a>
                              <p class="kad_tagline belowlogo-text">Donate an Hour</p>
                         </div> <!-- Close #logo -->
       </div><!-- close logo span -->
                <div class="col-md-12 kad-header-right">
           <nav id="nav-main" class="clearfix" role="navigation">
              <ul id="menu-home" class="sf-menu"><li  class="menu-home menu-item-377"><a href="http://donateanhour.org/">Home</a></li>
<li  class="menu-programs current-page-ancestor current-menu-ancestor current-menu-parent current-page-parent current_page_parent current_page_ancestor sf-dropdown menu-item-117"><a href="http://localhost/programs/">Programs</a>
<ul class="sf-dropdown-menu">
	<li  class="menu-utthan-%e0%a4%89%e0%a4%a4%e0%a5%8d%e0%a4%a5%e0%a4%be%e0%a4%a8 current-menu-item current_page_item menu-item-388"><a href="http://localhost/programs/uthaan/">Utthan (उत्थान)</a></li>
	<li  class="menu-my-learning-space menu-item-389"><a href="http://localhost/programs/my-learning-space/">My learning space</a></li>
	<li  class="menu-yashoda menu-item-387"><a href="http://localhost/programs/yashoda/">Yashoda</a></li>
</ul>
</li>
<li  class="menu-events sf-dropdown menu-item-155"><a href="http://localhost/events/">Events</a>
<ul class="sf-dropdown-menu">
	<li  class="menu-corporate-events sf-dropdown-submenu menu-item-836"><a href="http://localhost/corporate-events/">Corporate Events</a>
<ul class="sf-dropdown-menu">
		<li  class="menu-ihs-team-visit menu-item-1109"><a href="http://localhost/1107-2/">IHS Team Visit</a></li>
		<li  class="menu-christmas-party-at-mercer menu-item-993"><a href="http://localhost/events/christmas-party-at-mercer/">Christmas Party at Mercer</a></li>
		<li  class="menu-wall-painting-with-american-express menu-item-982"><a href="http://localhost/events/paint-the-wall-with-american-express/">Wall painting with American Express</a></li>
		<li  class="menu-tata-consultancy-services-tcs-team-visit menu-item-889"><a href="http://localhost/corporate-events/887-2/">Tata Consultancy Services (TCS) team visit</a></li>
		<li  class="menu-vodafone-awareness-camp menu-item-838"><a href="http://localhost/events/vodafone-awareness-camp/">Vodafone &#8211; awareness camp</a></li>
		<li  class="menu-paras-world-school-visit menu-item-932"><a href="http://localhost/corporate-events/paras-world-school-visit/">Paras World School visit</a></li>
	</ul>
</li>
	<li  class="menu-doar-events sf-dropdown-submenu menu-item-843"><a href="http://localhost/events/doar-events/">DoaR Events</a>
<ul class="sf-dropdown-menu">
		<li  class="menu-proud-to-be-an-indian menu-item-1031"><a href="http://localhost/events/proud-to-be-an-indian/">Proud to be an Indian</a></li>
		<li  class="menu-diwali-dhamaka menu-item-903"><a href="http://localhost/events/diwali-dhamaka-%e0%a4%a6%e0%a5%80%e0%a4%b5%e0%a4%be%e0%a4%b2%e0%a5%80-%e0%a4%a7%e0%a4%be%e0%a4%ae%e0%a4%be%e0%a4%95%e0%a4%be/">Diwali Dhamaka</a></li>
		<li  class="menu-dussehra-dhoom menu-item-820"><a href="http://localhost/events/%e0%a4%a6%e0%a4%b6%e0%a4%b9%e0%a4%b0%e0%a4%be-%e0%a4%a7%e0%a5%82%e0%a4%ae/">Dussehra Dhoom</a></li>
		<li  class="menu-international-day-of-non-violence menu-item-790"><a href="http://localhost/events/international-day-of-non-violence/">International Day of Non-Violence</a></li>
		<li  class="menu-global-connect-initiative menu-item-698"><a href="http://localhost/events/global-connect-initiative/">Global Connect Initiative</a></li>
		<li  class="menu-run-to-save-the-girl-child menu-item-684"><a href="http://localhost/events/run-to-save-girl-child/">Run To Save The Girl Child</a></li>
		<li  class="menu-tricolour-celebrations menu-item-648"><a href="http://localhost/events/646-2/">Tricolour celebrations</a></li>
		<li  class="menu-celebrating-life menu-item-589"><a href="http://localhost/celebrating-life/">Celebrating Life</a></li>
		<li  class="menu-idea-of-a-perfect-sunday menu-item-540"><a href="http://localhost/events/idea-of-a-perfect-sunday/">Idea of a perfect Sunday</a></li>
		<li  class="menu-rainy-yoga menu-item-470"><a href="http://localhost/events/rainy-yoga/">Rainy yoga</a></li>
		<li  class="menu-world-environment-day-2015 menu-item-297"><a href="http://localhost/events/world-environment-day-2015/">World Environment Day 2015</a></li>
		<li  class="menu-god-made-you-special menu-item-231"><a href="http://localhost/events/birthday-blast-god-made-you-special/">God made you special</a></li>
		<li  class="menu-sunday-library menu-item-391"><a href="http://localhost/events/sunday-library/">Sunday library</a></li>
		<li  class="menu-sunday-painting-day menu-item-392"><a href="http://localhost/events/sunday-painting-day/">Sunday painting day</a></li>
		<li  class="menu-playday-sunday menu-item-169"><a href="http://localhost/events/playday-sunday/">Playday Sunday</a></li>
		<li  class="menu-a-day-in-delhi-zoo menu-item-163"><a href="http://localhost/events/a-day-in-delhi-zoo/">A day in Delhi zoo</a></li>
	</ul>
</li>
</ul>
</li>
<li  class="menu-about-us sf-dropdown menu-item-521"><a href="http://localhost/about-us/">About Us</a>
<ul class="sf-dropdown-menu">
	<li  class="menu-introduction menu-item-390"><a href="http://localhost/about/">Introduction</a></li>
	<li  class="menu-team menu-item-1080"><a href="http://localhost/1050-2/">Team</a></li>
	<li  class="menu-locations menu-item-955"><a href="http://localhost/about-us/locations/">Locations</a></li>
	<li  class="menu-in-news menu-item-826"><a href="http://localhost/about-us/in-news/">In News</a></li>
</ul>
</li>
<li  class="menu-get-involved sf-dropdown menu-item-874"><a href="http://localhost/get-involved/">Get Involved</a>
<ul class="sf-dropdown-menu">
	<li  class="menu-workshops-with-children menu-item-967"><a href="http://localhost/get-involved/workshops-with-children/">Workshops with Children</a></li>
	<li  class="menu-participate menu-item-453"><a href="http://localhost/contributions/">Participate</a></li>
	<li  class="menu-contact-us menu-item-115"><a href="http://localhost/contact-us/">Contact Us</a></li>
</ul>
</li>
</ul>           </nav> 
          </div> <!-- Close menuclass-->
               
    </div> <!-- Close Row -->
               <div id="mobile-nav-trigger" class="nav-trigger">
              <button class="nav-trigger-case mobileclass collapsed" data-toggle="collapse" data-target=".kad-nav-collapse">
                <span class="kad-navbtn"><i class="icon-reorder"></i></span>
                <span class="kad-menu-name">Menu</span>
              </button>
            </div>
            <div id="kad-mobile-nav" class="kad-mobile-nav">
              <div class="kad-nav-inner mobileclass">
                <div class="kad-nav-collapse">
                <ul id="menu-home-1" class="kad-mnav"><li  class="menu-home menu-item-377"><a href="http://donateanhour.org/">Home</a></li>
<li  class="menu-programs current-page-ancestor current-menu-ancestor current-menu-parent current-page-parent current_page_parent current_page_ancestor sf-dropdown sf-dropdown-toggle menu-item-117"><a href="http://localhost/programs/">Programs</a><span class="kad-submenu-accordion collapse-next  kad-submenu-accordion-open" data-parent=".kad-nav-collapse" data-toggle="collapse" data-target=""><i class="icon-chevron-down"></i><i class="icon-chevron-up"></i></span>
<ul class="sf-dropdown-menu collapse">
	<li  class="menu-utthan current-menu-item current_page_item menu-item-388"><a href="http://localhost/programs/uthaan/">Utthan (उत्थान)</a></li>
	<li  class="menu-mylearningspace menu-item-389"><a href="http://localhost/programs/my-learning-space/">My learning space</a></li>
	<li  class="menu-yashoda menu-item-387"><a href="http://localhost/programs/yashoda/">Yashoda</a></li>
</ul>
</li>
<li  class="menu-events sf-dropdown sf-dropdown-toggle menu-item-155"><a href="http://localhost/events/">Events</a><span class="kad-submenu-accordion collapse-next  kad-submenu-accordion-open" data-parent=".kad-nav-collapse" data-toggle="collapse" data-target=""><i class="icon-chevron-down"></i><i class="icon-chevron-up"></i></span>
<ul class="sf-dropdown-menu collapse">
	<li  class="menu-corporateevents sf-dropdown-submenu sf-dropdown-toggle menu-item-836"><a href="http://localhost/corporate-events/">Corporate Events</a><span class="kad-submenu-accordion collapse-next  kad-submenu-accordion-open" data-parent=".kad-nav-collapse" data-toggle="collapse" data-target=""><i class="icon-chevron-down"></i><i class="icon-chevron-up"></i></span>
<ul class="sf-dropdown-menu collapse">
		<li  class="menu-ihsteamvisit menu-item-1109"><a href="http://localhost/1107-2/">IHS Team Visit</a></li>
		<li  class="menu-christmaspartyatmercer menu-item-993"><a href="http://localhost/events/christmas-party-at-mercer/">Christmas Party at Mercer</a></li>
		<li  class="menu-wallpaintingwithamericanexpress menu-item-982"><a href="http://localhost/events/paint-the-wall-with-american-express/">Wall painting with American Express</a></li>
		<li  class="menu-tataconsultancyservicestcsteamvisit menu-item-889"><a href="http://localhost/corporate-events/887-2/">Tata Consultancy Services (TCS) team visit</a></li>
		<li  class="menu-vodafone-awarenesscamp menu-item-838"><a href="http://localhost/events/vodafone-awareness-camp/">Vodafone &#8211; awareness camp</a></li>
		<li  class="menu-parasworldschoolvisit menu-item-932"><a href="http://localhost/corporate-events/paras-world-school-visit/">Paras World School visit</a></li>
	</ul>
</li>
	<li  class="menu-doarevents sf-dropdown-submenu sf-dropdown-toggle menu-item-843"><a href="http://localhost/events/doar-events/">DoaR Events</a><span class="kad-submenu-accordion collapse-next  kad-submenu-accordion-open" data-parent=".kad-nav-collapse" data-toggle="collapse" data-target=""><i class="icon-chevron-down"></i><i class="icon-chevron-up"></i></span>
<ul class="sf-dropdown-menu collapse">
		<li  class="menu-proudtobeanindian menu-item-1031"><a href="http://localhost/events/proud-to-be-an-indian/">Proud to be an Indian</a></li>
		<li  class="menu-diwalidhamaka menu-item-903"><a href="http://localhost/events/diwali-dhamaka-%e0%a4%a6%e0%a5%80%e0%a4%b5%e0%a4%be%e0%a4%b2%e0%a5%80-%e0%a4%a7%e0%a4%be%e0%a4%ae%e0%a4%be%e0%a4%95%e0%a4%be/">Diwali Dhamaka</a></li>
		<li  class="menu-dussehradhoom menu-item-820"><a href="http://localhost/events/%e0%a4%a6%e0%a4%b6%e0%a4%b9%e0%a4%b0%e0%a4%be-%e0%a4%a7%e0%a5%82%e0%a4%ae/">Dussehra Dhoom</a></li>
		<li  class="menu-internationaldayofnon-violence menu-item-790"><a href="http://localhost/events/international-day-of-non-violence/">International Day of Non-Violence</a></li>
		<li  class="menu-globalconnectinitiative menu-item-698"><a href="http://localhost/events/global-connect-initiative/">Global Connect Initiative</a></li>
		<li  class="menu-runtosavethegirlchild menu-item-684"><a href="http://localhost/events/run-to-save-girl-child/">Run To Save The Girl Child</a></li>
		<li  class="menu-tricolourcelebrations menu-item-648"><a href="http://localhost/events/646-2/">Tricolour celebrations</a></li>
		<li  class="menu-celebratinglife menu-item-589"><a href="http://localhost/celebrating-life/">Celebrating Life</a></li>
		<li  class="menu-ideaofaperfectsunday menu-item-540"><a href="http://localhost/events/idea-of-a-perfect-sunday/">Idea of a perfect Sunday</a></li>
		<li  class="menu-rainyyoga menu-item-470"><a href="http://localhost/events/rainy-yoga/">Rainy yoga</a></li>
		<li  class="menu-worldenvironmentday2015 menu-item-297"><a href="http://localhost/events/world-environment-day-2015/">World Environment Day 2015</a></li>
		<li  class="menu-godmadeyouspecial menu-item-231"><a href="http://localhost/events/birthday-blast-god-made-you-special/">God made you special</a></li>
		<li  class="menu-sundaylibrary menu-item-391"><a href="http://localhost/events/sunday-library/">Sunday library</a></li>
		<li  class="menu-sundaypaintingday menu-item-392"><a href="http://localhost/events/sunday-painting-day/">Sunday painting day</a></li>
		<li  class="menu-playdaysunday menu-item-169"><a href="http://localhost/events/playday-sunday/">Playday Sunday</a></li>
		<li  class="menu-adayindelhizoo menu-item-163"><a href="http://localhost/events/a-day-in-delhi-zoo/">A day in Delhi zoo</a></li>
	</ul>
</li>
</ul>
</li>
<li  class="menu-aboutus sf-dropdown sf-dropdown-toggle menu-item-521"><a href="http://localhost/about-us/">About Us</a><span class="kad-submenu-accordion collapse-next  kad-submenu-accordion-open" data-parent=".kad-nav-collapse" data-toggle="collapse" data-target=""><i class="icon-chevron-down"></i><i class="icon-chevron-up"></i></span>
<ul class="sf-dropdown-menu collapse">
	<li  class="menu-introduction menu-item-390"><a href="http://localhost/about/">Introduction</a></li>
	<li  class="menu-team menu-item-1080"><a href="http://localhost/1050-2/">Team</a></li>
	<li  class="menu-locations menu-item-955"><a href="http://localhost/about-us/locations/">Locations</a></li>
	<li  class="menu-innews menu-item-826"><a href="http://localhost/about-us/in-news/">In News</a></li>
</ul>
</li>
<li  class="menu-getinvolved sf-dropdown sf-dropdown-toggle menu-item-874"><a href="http://localhost/get-involved/">Get Involved</a><span class="kad-submenu-accordion collapse-next  kad-submenu-accordion-open" data-parent=".kad-nav-collapse" data-toggle="collapse" data-target=""><i class="icon-chevron-down"></i><i class="icon-chevron-up"></i></span>
<ul class="sf-dropdown-menu collapse">
	<li  class="menu-workshopswithchildren menu-item-967"><a href="http://localhost/get-involved/workshops-with-children/">Workshops with Children</a></li>
	<li  class="menu-participate menu-item-453"><a href="http://localhost/contributions/">Participate</a></li>
	<li  class="menu-contactus menu-item-115"><a href="http://localhost/contact-us/">Contact Us</a></li>
</ul>
</li>
</ul>               </div>
            </div>
          </div>   
           
  </div> <!-- Close Container -->
   
     </header>      <div class="wrap contentclass" role="document">

      
<div id="pageheader" class="titleclass">
	<div class="container">
		<div class="page-header">
	<h1 class="entry-title" itemprop="name">
		Utthan (उत्थान)	</h1>
   	</div>	</div><!--container-->
</div><!--titleclass-->
	
<div id="content" class="container">
   	<div class="row">
     	<div class="main col-md-12" role="main">
				<div class="entry-content" itemprop="mainContentOfPage">
					  <h5><strong><em><span style="color: #808080;">&#8220;We rise by lifting others&#8221;</span></em></strong></h5>
<p><strong>Overview</strong></p>
<p style="text-align: justify;">Utthan means upliftment. This is our flagship program that aims to provide supplementary of parental support to the children from poor and disadvantaged socio-economic background. Under this program volunteers across Delhi NCR run daily or weekend classes after or before their office hours and mentor underprivileged children. Some volunteers provide home tuitions as well.</p>
<p style="text-align: justify;"><strong>Focus group</strong></p>
<p style="text-align: justify;">We categorise children in 4 groups-</p>
<ol>
<li>Children of migrants who have dropped studies as they are not getting admission anywhere due to lack of the documents.</li>
<li>Children of migrants who got admission in the schools but
<ul>
<li>they are enrolled in a lower class (std) as the schools didn’t find them suitable for the class where they were in their previous school in their village; or</li>
<li>they are not regular as they are involved in child labour and now out of the regular education system ( school system); or</li>
<li>they are not attending any school as they are dismissed due to their absence of more than 15 days due to their visit to their village with their parents.</li>
</ul>
</li>
<li>Children of migrants who have dropped their studies as their parents have no fix job and a place to live and that is the reason even government schools don’t enrol them easily and in most of the cases they remain illiterate like their parents and look after their younger siblings and forced for the child labour.</li>
<li>Children who are completely out of the education system as they have crossed the age limit for enrolment in primary schools.</li>
</ol>
<p style="text-align: justify;"><strong>What we do</strong></p>
<ol>
<li style="text-align: justify;">We run daily evening classes after office hours for the focus group.</li>
<li style="text-align: justify;">We focus on making study an interesting thing in their life so they eventually develop an interest in what they are learning and remain motivated to join the regular school.</li>
<li style="text-align: justify;">We teach the concepts with help of toys and trash.</li>
<li style="text-align: justify;">Once in a week we council parents of the focus group.</li>
<li style="text-align: justify;">We survey the local area and encourage underprivileged children to attend our classes regularly.</li>
</ol>
<p><strong>Locations</strong></p>
<p>May volunteers are running Donate an Hour learning centres and also giving home tuition at their home. Please locate us on the google map and let us know if you are also interested to take-up this activity.</p>
<a href="https://www.google.com/maps/d/u/0/viewer?mid=zA-i1VxXkZHA.kyfpePPU2vrs" class="kad-btn kad-btn-primary" id="kadbtn61" target="_blank" style="color:#FFF;">Uthhan project in Gurgaon/Delhi NCR</a>
<p>&nbsp;</p>
<p>&nbsp;</p>
  				</div>
						</div><!-- /.main -->            
                    </div><!-- /.row-->
        </div><!-- /.content -->
      </div><!-- /.wrap -->
      <footer id="containerfooter" class="footerclass" role="contentinfo">
  <div class="container">
  	<div class="row">
  				        		                </div>
        <div class="footercredits clearfix">
    		
    		        	<p>&copy; 2015 Donate an Hour | All Right Reserved.</p>
    	</div>

  </div>

</footer>

<script type="text/javascript">jQuery(document).ready(function ($) {var magnificPopupEnabled = false;$.extend(true, $.magnificPopup.defaults, {disableOn: function() {return false;}});});</script><script type='text/javascript' src='http://localhost/wp-includes/js/admin-bar.min.js?ver=4.4.2'></script>
<script type='text/javascript' src='http://localhost/wp-includes/js/masonry.min.js?ver=3.1.2'></script>
<script type='text/javascript' src='http://localhost/wp-content/themes/virtue/assets/js/min/plugins-min.js?ver=257'></script>
<script type='text/javascript' src='http://localhost/wp-content/themes/virtue/assets/js/main.js?ver=257'></script>
<script type='text/javascript' src='http://localhost/wp-includes/js/wp-embed.min.js?ver=4.4.2'></script>
	<script type="text/javascript">
		(function() {
			var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');

			request = true;

			b[c] = b[c].replace( rcs, ' ' );
			b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
		}());
	</script>
			<div id="wpadminbar" class="nojq nojs">
							<a class="screen-reader-shortcut" href="#wp-toolbar" tabindex="1">Skip to toolbar</a>
						<div class="quicklinks" id="wp-toolbar" role="navigation" aria-label="Toolbar" tabindex="0">
				<ul id="wp-admin-bar-root-default" class="ab-top-menu">
		<li id="wp-admin-bar-wp-logo" class="menupop"><a class="ab-item"  aria-haspopup="true" href="http://localhost/wp-admin/about.php"><span class="ab-icon"></span><span class="screen-reader-text">About WordPress</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-wp-logo-default" class="ab-submenu">
		<li id="wp-admin-bar-about"><a class="ab-item"  href="http://localhost/wp-admin/about.php">About WordPress</a>		</li></ul><ul id="wp-admin-bar-wp-logo-external" class="ab-sub-secondary ab-submenu">
		<li id="wp-admin-bar-wporg"><a class="ab-item"  href="https://wordpress.org/">WordPress.org</a>		</li>
		<li id="wp-admin-bar-documentation"><a class="ab-item"  href="https://codex.wordpress.org/">Documentation</a>		</li>
		<li id="wp-admin-bar-support-forums"><a class="ab-item"  href="https://wordpress.org/support/">Support Forums</a>		</li>
		<li id="wp-admin-bar-feedback"><a class="ab-item"  href="https://wordpress.org/support/forum/requests-and-feedback">Feedback</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-site-name" class="menupop"><a class="ab-item"  aria-haspopup="true" href="http://localhost/wp-admin/">Donate an Hour ( DoaR India)</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-site-name-default" class="ab-submenu">
		<li id="wp-admin-bar-dashboard"><a class="ab-item"  href="http://localhost/wp-admin/">Dashboard</a>		</li></ul><ul id="wp-admin-bar-appearance" class="ab-submenu">
		<li id="wp-admin-bar-themes"><a class="ab-item"  href="http://localhost/wp-admin/themes.php">Themes</a>		</li>
		<li id="wp-admin-bar-widgets"><a class="ab-item"  href="http://localhost/wp-admin/widgets.php">Widgets</a>		</li>
		<li id="wp-admin-bar-menus"><a class="ab-item"  href="http://localhost/wp-admin/nav-menus.php">Menus</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-customize" class="hide-if-no-customize"><a class="ab-item"  href="http://localhost/wp-admin/customize.php?url=http%3A%2F%2Flocalhost%2Fprograms%2Futhaan%2F">Customise</a>		</li>
		<li id="wp-admin-bar-ktoptions"><a class="ab-item"  href="http://localhost/wp-admin/admin.php?page=ktoptions"><span class="ab-icon dashicons-admin-generic"></span>Theme Options</a>		</li>
		<li id="wp-admin-bar-updates"><a class="ab-item"  href="http://localhost/wp-admin/update-core.php" title="1 Plugin Update, 12 Theme Updates"><span class="ab-icon"></span><span class="ab-label">13</span><span class="screen-reader-text">1 Plugin Update, 12 Theme Updates</span></a>		</li>
		<li id="wp-admin-bar-comments"><a class="ab-item"  href="http://localhost/wp-admin/edit-comments.php" title="0 comments awaiting moderation"><span class="ab-icon"></span><span id="ab-awaiting-mod" class="ab-label awaiting-mod pending-count count-0">0</span></a>		</li>
		<li id="wp-admin-bar-new-content" class="menupop"><a class="ab-item"  aria-haspopup="true" href="http://localhost/wp-admin/post-new.php"><span class="ab-icon"></span><span class="ab-label">New</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-new-content-default" class="ab-submenu">
		<li id="wp-admin-bar-new-post"><a class="ab-item"  href="http://localhost/wp-admin/post-new.php">Post</a>		</li>
		<li id="wp-admin-bar-new-media"><a class="ab-item"  href="http://localhost/wp-admin/media-new.php">Media</a>		</li>
		<li id="wp-admin-bar-new-link"><a class="ab-item"  href="http://localhost/wp-admin/link-add.php">Link</a>		</li>
		<li id="wp-admin-bar-new-page"><a class="ab-item"  href="http://localhost/wp-admin/post-new.php?post_type=page">Page</a>		</li>
		<li id="wp-admin-bar-new-team_member"><a class="ab-item"  href="http://localhost/wp-admin/post-new.php?post_type=team_member">Team Member</a>		</li>
		<li id="wp-admin-bar-new-team"><a class="ab-item"  href="http://localhost/wp-admin/post-new.php?post_type=team">Team</a>		</li>
		<li id="wp-admin-bar-new-portfolio"><a class="ab-item"  href="http://localhost/wp-admin/post-new.php?post_type=portfolio">Portfolio Item</a>		</li>
		<li id="wp-admin-bar-new-user"><a class="ab-item"  href="http://localhost/wp-admin/user-new.php">User</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-edit"><a class="ab-item"  href="http://localhost/wp-admin/post.php?post=31&#038;action=edit">Edit Page</a>		</li>
		<li id="wp-admin-bar-wpseo-menu" class="menupop"><a class="ab-item"  aria-haspopup="true" href="http://localhost/wp-admin/post.php?post=31&#038;action=edit">SEO<div title="N/A" class="wpseo-score-icon na 0"></div></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-wpseo-menu-default" class="ab-submenu">
		<li id="wp-admin-bar-wpseo-kwresearch" class="menupop"><div class="ab-item ab-empty-item"  aria-haspopup="true">Keyword Research</div><div class="ab-sub-wrapper"><ul id="wp-admin-bar-wpseo-kwresearch-default" class="ab-submenu">
		<li id="wp-admin-bar-wpseo-adwordsexternal"><a class="ab-item"  href="http://adwords.google.com/keywordplanner" target="_blank">AdWords External</a>		</li>
		<li id="wp-admin-bar-wpseo-googleinsights"><a class="ab-item"  href="http://www.google.com/insights/search/#q=&#038;cmpt=q" target="_blank">Google Insights</a>		</li>
		<li id="wp-admin-bar-wpseo-wordtracker"><a class="ab-item"  href="http://tools.seobook.com/keyword-tools/seobook/?keyword=" target="_blank">SEO Book</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-wpseo-analysis" class="menupop"><div class="ab-item ab-empty-item"  aria-haspopup="true">Analyse this page</div><div class="ab-sub-wrapper"><ul id="wp-admin-bar-wpseo-analysis-default" class="ab-submenu">
		<li id="wp-admin-bar-wpseo-inlinks-ose"><a class="ab-item"  href="//moz.com/researchtools/ose/links?site=http%3A%2F%2Flocalhost%2Fprograms%2Futhaan%2F" target="_blank">Check Inlinks (OSE)</a>		</li>
		<li id="wp-admin-bar-wpseo-kwdensity"><a class="ab-item"  href="//www.zippy.co.uk/keyworddensity/index.php?url=http%3A%2F%2Flocalhost%2Fprograms%2Futhaan%2F&#038;keyword=" target="_blank">Check Keyword Density</a>		</li>
		<li id="wp-admin-bar-wpseo-cache"><a class="ab-item"  href="//webcache.googleusercontent.com/search?strip=1&#038;q=cache:http%3A%2F%2Flocalhost%2Fprograms%2Futhaan%2F" target="_blank">Check Google Cache</a>		</li>
		<li id="wp-admin-bar-wpseo-header"><a class="ab-item"  href="//quixapp.com/headers/?r=http%3A%2F%2Flocalhost%2Fprograms%2Futhaan%2F" target="_blank">Check Headers</a>		</li>
		<li id="wp-admin-bar-wpseo-richsnippets"><a class="ab-item"  href="//www.google.com/webmasters/tools/richsnippets?q=http%3A%2F%2Flocalhost%2Fprograms%2Futhaan%2F" target="_blank">Check Rich Snippets</a>		</li>
		<li id="wp-admin-bar-wpseo-facebookdebug"><a class="ab-item"  href="//developers.facebook.com/tools/debug/og/object?q=http%3A%2F%2Flocalhost%2Fprograms%2Futhaan%2F" target="_blank">Facebook Debugger</a>		</li>
		<li id="wp-admin-bar-wpseo-pinterestvalidator"><a class="ab-item"  href="//developers.pinterest.com/rich_pins/validator/?link=http%3A%2F%2Flocalhost%2Fprograms%2Futhaan%2F" target="_blank">Pinterest Rich Pins Validator</a>		</li>
		<li id="wp-admin-bar-wpseo-htmlvalidation"><a class="ab-item"  href="//validator.w3.org/check?uri=http%3A%2F%2Flocalhost%2Fprograms%2Futhaan%2F" target="_blank">HTML Validator</a>		</li>
		<li id="wp-admin-bar-wpseo-cssvalidation"><a class="ab-item"  href="//jigsaw.w3.org/css-validator/validator?uri=http%3A%2F%2Flocalhost%2Fprograms%2Futhaan%2F" target="_blank">CSS Validator</a>		</li>
		<li id="wp-admin-bar-wpseo-pagespeed"><a class="ab-item"  href="//developers.google.com/speed/pagespeed/insights/?url=http%3A%2F%2Flocalhost%2Fprograms%2Futhaan%2F" target="_blank">Google Page Speed Test</a>		</li>
		<li id="wp-admin-bar-wpseo-modernie"><a class="ab-item"  href="//www.modern.ie/en-us/report#http%3A%2F%2Flocalhost%2Fprograms%2Futhaan%2F" target="_blank">Modern IE Site Scan</a>		</li>
		<li id="wp-admin-bar-wpseo-google-mobile-friendly"><a class="ab-item"  href="https://www.google.com/webmasters/tools/mobile-friendly/?url=http%3A%2F%2Flocalhost%2Fprograms%2Futhaan%2F" target="_blank">Mobile-Friendly Test</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-wpseo-settings" class="menupop"><div class="ab-item ab-empty-item"  aria-haspopup="true">SEO Settings</div><div class="ab-sub-wrapper"><ul id="wp-admin-bar-wpseo-settings-default" class="ab-submenu">
		<li id="wp-admin-bar-wpseo-general"><a class="ab-item"  href="http://localhost/wp-admin/admin.php?page=wpseo_dashboard">General</a>		</li>
		<li id="wp-admin-bar-wpseo-titles"><a class="ab-item"  href="http://localhost/wp-admin/admin.php?page=wpseo_titles">Titles and Metas</a>		</li>
		<li id="wp-admin-bar-wpseo-social"><a class="ab-item"  href="http://localhost/wp-admin/admin.php?page=wpseo_social">Social</a>		</li>
		<li id="wp-admin-bar-wpseo-xml"><a class="ab-item"  href="http://localhost/wp-admin/admin.php?page=wpseo_xml">XML Sitemaps</a>		</li>
		<li id="wp-admin-bar-wpseo-wpseo-advanced"><a class="ab-item"  href="http://localhost/wp-admin/admin.php?page=wpseo_advanced">Advanced</a>		</li>
		<li id="wp-admin-bar-wpseo-tools"><a class="ab-item"  href="http://localhost/wp-admin/admin.php?page=wpseo_tools">Tools</a>		</li>
		<li id="wp-admin-bar-wpseo-search-console"><a class="ab-item"  href="http://localhost/wp-admin/admin.php?page=wpseo_search_console">Search Console</a>		</li>
		<li id="wp-admin-bar-wpseo-licenses"><a class="ab-item"  href="http://localhost/wp-admin/admin.php?page=wpseo_licenses"><span style="color:#f18500">Extensions</span></a>		</li></ul></div>		</li></ul></div>		</li>
		<li id="wp-admin-bar-vc_inline-admin-bar-link" class="vc_inline-link"><a class="ab-item"  href="http://localhost/wp-admin/edit.php?vc_action=vc_inline&#038;post_id=31&#038;post_type=page">Edit with Visual Composer</a>		</li>
		<li id="wp-admin-bar-font_settings"><a class="ab-item"  href="#">Font settings</a>		</li></ul><ul id="wp-admin-bar-top-secondary" class="ab-top-secondary ab-top-menu">
		<li id="wp-admin-bar-search" class="admin-bar-search"><div class="ab-item ab-empty-item" tabindex="-1"><form action="http://localhost/" method="get" id="adminbarsearch"><input class="adminbar-input" name="s" id="adminbar-search" type="text" value="" maxlength="150" /><label for="adminbar-search" class="screen-reader-text">Search</label><input type="submit" class="adminbar-button" value="Search"/></form></div>		</li>
		<li id="wp-admin-bar-my-account" class="menupop"><a class="ab-item"  aria-haspopup="true" href="http://localhost/wp-admin/profile.php">How are you, IT team?</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-user-actions" class="ab-submenu">
		<li id="wp-admin-bar-user-info"><a class="ab-item" tabindex="-1" href="http://localhost/wp-admin/profile.php"><span class='display-name'>IT team</span></a>		</li>
		<li id="wp-admin-bar-edit-profile"><a class="ab-item"  href="http://localhost/wp-admin/profile.php">Edit My Profile</a>		</li>
		<li id="wp-admin-bar-logout"><a class="ab-item"  href="http://localhost/wp-login.php?action=logout&#038;_wpnonce=4ee4d4bcf9">Log Out</a>		</li></ul></div>		</li></ul>			</div>
						<a class="screen-reader-shortcut" href="http://localhost/wp-login.php?action=logout&#038;_wpnonce=4ee4d4bcf9">Log Out</a>
					</div>

		    </div><!--Wrapper-->
  </body>
</html>

<!-- Dynamic page generated in 0.249 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2016-03-19 11:29:05 -->
